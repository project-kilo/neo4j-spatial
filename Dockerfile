FROM neo4j:3.0.4
MAINTAINER Robin McCorkell "robin@mccorkell.me.uk"

COPY neo4j-spatial-0.23-neo4j-3.0.4-server-plugin.jar plugins/
