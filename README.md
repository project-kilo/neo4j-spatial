Dockerized Neo4j with Spatial plugin
====================================

Based on the official Neo4j Docker image.

Docker image at `registry.gitlab.com/project-kilo/neo4j-spatial`

`latest` tag always tracks `master` branch.

Usage
-----

```
docker login registry.gitlab.com
docker pull registry.gitlab.com/project-kilo/neo4j-spatial:latest
docker run -p 7474:7474 -p 7687:7687 registry.gitlab.com/project-kilo/neo4j-spatial:latest
```

Access `http://localhost:7474` for the Neo4j console, and connect applications
to `localhost:7687` for Bolt access.
